from django.conf.urls import patterns, include, url
from haystack.views import SearchView, search_view_factory
from haystack.forms import ModelSearchForm
from haystack.query import SearchQuerySet
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'searchengine.views.home', name='home'),
    url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

sqs = SearchQuerySet().filter(author='john')

urlpatterns += patterns('haystack.views',
    url(r'^$', search_view_factory(
        view_class=SearchView,
        template='search.html',
        searchqueryset=sqs,
        form_class=ModelSearchForm
    ), name='haystack_search'),
)