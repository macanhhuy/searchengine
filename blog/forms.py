from haystack.forms import SearchForm
from django.conf import settings
from django import forms
from django.contrib.auth.models import User
from blog.models import Note

LIMIT = getattr(settings, 'HAYSTACK_MAX_RESULTS', 50)

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note

    def clean_user(self):
        if not self.cleaned_data['user']:
            return User()
        return self.cleaned_data['user']


class NotesSearchForm(SearchForm):

    def no_query_found(self):
        return self.searchqueryset.all()[:LIMIT]


class DateRangeSearchForm(SearchForm):
    start_date = forms.DateField(required=False)
    end_date = forms.DateField(required=False)

    def search(self):
        # First, store the SearchQuerySet received from other processing.
        sqs = super(DateRangeSearchForm, self).search()

        if not self.is_valid():
            return self.no_query_found()

        # Check to see if a start_date was chosen.
        if self.cleaned_data['start_date']:
            sqs = sqs.filter(pub_date__gte=self.cleaned_data['start_date'])

        # Check to see if an end_date was chosen.
        if self.cleaned_data['end_date']:
            sqs = sqs.filter(pub_date__lte=self.cleaned_data['end_date'])

        return sqs