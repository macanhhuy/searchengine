#-*- coding: utf-8 -*-
from django.contrib import admin
from blog.models import *
from .forms import NoteForm

class NoteAdmin(admin.ModelAdmin):
    #form = NoteForm

    date_hierarchy = 'pub_date'
    search_fields = ('title', 'body')
    list_display = ('title', 'pub_date', 'user')
    fieldsets = ((
        None, {
            'fields': ('title', 'body', 'pub_date')
        }),
    )
    
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class BlogAdmin(admin.ModelAdmin):
    #form = NoteForm
    change_form_template = 'blog/admin/change_form.html'
    date_hierarchy = 'pub_date'
    search_fields = ('title', 'body')
    list_display = ('title', 'pub_date', 'user')
    fieldsets = ((
        None, {
            'fields': ('title', 'body', 'pub_date')
        }),
    )
    
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

admin.site.register(Blog, BlogAdmin)
admin.site.register(Note, NoteAdmin)
admin.site.register(MyModel)
admin.site.register(RelatedM2M)
admin.site.register(RelatedModel)

