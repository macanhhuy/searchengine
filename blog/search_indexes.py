import datetime
from haystack import indexes
from blog.models import Note, Blog, MyModel

class NoteIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True)
	title = indexes.CharField(model_attr='title', boost=1.125)
	body = indexes.CharField(model_attr='body')
	pub_date = indexes.DateTimeField(model_attr='pub_date')
	title_auto = indexes.EdgeNgramField(model_attr='title')
	body_auto = indexes.EdgeNgramField(model_attr='body')

	def get_model(self):
		return Note

	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())


class BlogIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.CharField(document=True, use_template=True)
	title = indexes.CharField(model_attr='title', boost=1.125)
	body = indexes.CharField(model_attr='body')
	pub_date = indexes.DateTimeField(model_attr='pub_date')
	title_auto = indexes.EdgeNgramField(model_attr='title')
	body_auto = indexes.EdgeNgramField(model_attr='body')

	def get_model(self):
		return Blog

	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())		

class MyModelIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, model_attr="field_1")
    title = indexes.CharField(model_attr='title_field')
    multi = indexes.MultiValueField()

    def get_model(self):
        return MyModel

	def prepare_multi(self, obj):
	    if obj.multi.related_m2m.count():
	        return [p.pk for p in obj.multi.related_m2m.all()]
	    return [u"Default"]

    def index_queryset(self):
        return self.get_model().objects.all()