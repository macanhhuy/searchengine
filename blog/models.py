#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import signals
#from pyuploadcare.dj import ImageField

MAX_LENGTH = 255

class RelatedM2M(models.Model):
    name = models.CharField(max_length=MAX_LENGTH, primary_key=True)


class RelatedModel(models.Model):
    name = models.CharField(max_length=MAX_LENGTH, primary_key=True)
    related_m2m = models.ManyToManyField(RelatedM2M, blank=True, null=True)

class MyModel(models.Model):
    field_1 = models.TextField()
    title_field = models.CharField(max_length=MAX_LENGTH)
    multi = models.ForeignKey(RelatedModel)

        
class PublicManager(models.Manager):
    """Returns published articles that are not in the future."""
    
    def published(self):
        return self.get_query_set().filter(pub_date__lte=datetime.now())

class Note(models.Model):
	user = models.ForeignKey(User, related_name='user_notes')
	title = models.CharField(max_length=200)
	body = models.TextField()
	pub_date = models.DateTimeField(default=timezone.now)
	objects = PublicManager()
    
	def __unicode__(self):
		return self.title

class Blog(models.Model):
	user = models.ForeignKey(User, related_name='user_blogs')
	title = models.CharField(max_length=200)
	body = models.TextField()
	#thumbnail = ImageField(blank=True, manual_crop="")
	pub_date = models.DateTimeField(default=timezone.now)
	objects = PublicManager()
    
	def __unicode__(self):
		return self.title
	class Meta:
		db_table = 'blogs'	


