from django.shortcuts import render
from forms import NotesSearchForm
from django.shortcuts import render_to_response
import json
from django.http import HttpResponse
from haystack.query import SearchQuerySet
from blog.models import Note, Blog

def autocomplete(request):
    # sqs1 = SearchQuerySet().autocomplete(body_auto=request.GET.get('q', ''))
    # sqs2 = SearchQuerySet().autocomplete(title_auto=request.GET.get('q', ''))
    # sqs = sqs1 | sqs2
    q = request.GET.get('q', '')
    t = request.GET.get('t', 'note')
    if t == 'note':
        sqs = SearchQuerySet().models(Note)
    else:
        sqs = SearchQuerySet().models(Blog)
    sqs1 = sqs.filter(title_auto=q)
    sqs2 = sqs.filter(body_auto=q)
    sqs = sqs1 | sqs2
    suggestions = [result.title for result in sqs]
    # Make sure you return a JSON object, not a bare list.
    # Otherwise, you could be vulnerable to an XSS attack.
    the_data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(the_data, content_type='application/json')

def search(request):
    return render_to_response('search.html', {})

def notes(request):
    form = NotesSearchForm(request.GET)
    notes = form.search()
    return render_to_response('search/search_root.html', {'notes': notes, 'query': request.GET.get('q', '')})