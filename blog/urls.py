from django.conf.urls import patterns, include, url

urlpatterns = patterns('blog.views',

    url(r'^$', 'notes', name='notes'),
    url(r'search$', 'search', name='search'),
    url(r'autocomplete$', 'autocomplete', name='autocomplete'),

)
